from datetime import datetime, UTC
from enum import Enum, auto
from typing import Optional, Self, cast

import click
from flask import abort, current_app, session
from werkzeug.security import generate_password_hash, check_password_hash

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey, orm
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship

import fsrs


class Base(DeclarativeBase):
  pass


db = SQLAlchemy(model_class=Base)


class Role(Enum):
    ADMIN = auto()
    TEACHER = auto()
    STUDENT = auto()


class User(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    moodle_name: Mapped[str] = mapped_column(unique=True, nullable=False)
    repetition: Mapped[list["LearningState"]] = relationship(back_populates="user")
    password: Mapped[str]
    role: Mapped[Role] = mapped_column(default=Role.STUDENT)

    def __init__(self, *, moodle_name:str, password:str, role:Role = Role.STUDENT) -> None:
        super().__init__(moodle_name=moodle_name, password=generate_password_hash(password), role=role) # type: ignore

    def check_password(self, password) -> bool:
        return check_password_hash(self.password, password)
    
    @classmethod
    def user_or_401(cls) -> Self:
        if "user" not in session:
            abort(401, "not connected")
        
        user:Self|None = cast(Self|None, cls.query.filter_by(moodle_name=session["user"]).one_or_none())

        if user is None:
            del session["user"]
            abort(401, "not connected")
        
        return user

    @classmethod
    def connect(cls, username:str, passwd:str) -> Self|None:
        user:Self|None = cast(Self|None, cls.query.filter_by(moodle_name=username).one_or_none())
        if user is None or not user.check_password(passwd):
            return None

        session["user"] = user.moodle_name
        return user
    
    @staticmethod
    def disconnect() -> None:
        if "user" not in session:
            return
        
        del session["user"]

    @staticmethod
    def connected() -> bool:
        return "user" in session


class Card(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    name: Mapped[str] = mapped_column(unique=True)
    question: Mapped[str]
    answer: Mapped[str]

    def __init__(self,*, name:str, question:str, answer:str, id:Optional[int]=None) -> None:
        if id is not None:
            super().__init__(id=id, name=name, question=question, answer=answer) # type: ignore
        else:
            super().__init__(name=name, question=question, answer=answer) # type: ignore

    repetition: Mapped[list["LearningState"]] = relationship(back_populates="card")


class LearningState(db.Model):
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
    user: Mapped[User] = relationship(back_populates="repetition")
    card_id: Mapped[int] = mapped_column(ForeignKey("card.id"))
    card: Mapped[Card] = relationship(back_populates="repetition")

    due: Mapped[datetime] = mapped_column(default=lambda: datetime.now(UTC))
    stability: Mapped[float] = mapped_column(default=0)
    difficulty: Mapped[float] = mapped_column(default=0)
    elapsed_days: Mapped[int] = mapped_column(default=0)
    scheduled_days: Mapped[int] = mapped_column(default=0)
    reps: Mapped[int] = mapped_column(default=0)
    lapses: Mapped[int] = mapped_column(default=0)
    state: Mapped[int] = mapped_column(default=0)
    last_review: Mapped[Optional[datetime]] = mapped_column(default=None)

    def __init__(self,*, user, card) -> None:
        super().__init__(user=user, card=card)  # type: ignore

    @orm.reconstructor
    def init_on_load(self):
        self.due = self.due.replace(tzinfo=UTC)
        if self.last_review is not None:
            self.last_review = self.last_review.replace(tzinfo=UTC)


    def update_from_fsrs(self, card:fsrs.Card) -> None:
        self.due = card.due
        self.stability = card.stability
        self.elapsed_days = card.elapsed_days
        self.scheduled_days = card.scheduled_days
        self.reps = card.reps
        self.lapses = card.lapses
        self.state = card.state
        self.last_review = card.last_review
        

    def to_fsrs(self) -> fsrs.Card:
        card = fsrs.Card(
            due=self.due,
            stability=self.stability, # type: ignore
            difficulty=self.difficulty, # type: ignore
            elapsed_days=self.elapsed_days,
            scheduled_days=self.scheduled_days,
            reps=self.reps,
            lapses=self.lapses,
            state=fsrs.State(self.state)
        )
        if self.last_review is not None:
            card.last_review = self.last_review
        return card


def init_db():
    with current_app.app_context():
        db.create_all()


@click.command('init-db')
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the database.')

