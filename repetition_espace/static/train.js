const answer_level = ["good", "easy", "hard", "again"]

function show_answer() {
    document.getElementById("answer").classList.remove("invisible")
}

function answer(url, level_id) {
    document.getElementById("next").classList.remove("invisible")
    for(const level of answer_level) {
        document.getElementById(level).removeAttribute("disabled")
    }
    document.getElementById(level_id).setAttribute("disabled", "disabled")

    let data = new FormData()
    data.append("level", level_id)

    fetch(url, {
        "method": "POST",
        "body": data
    }).then()
}