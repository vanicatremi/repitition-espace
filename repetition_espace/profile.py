from typing import Optional, assert_type, cast
from flask import Blueprint, abort, current_app, flash, g, redirect, render_template, request, session, url_for
from werkzeug import Response

from .db import Role, db, User

bp = Blueprint("profile", __name__, url_prefix="/profile")

@bp.route('/login')
@bp.route('/')
def login() -> str|Response:
    user:Optional[User] = User.query.filter(User.role == Role.ADMIN).first()
    if user is None:
        return render_template("auth/first-admin-signup.html")

    if "eleve" not in request.args:
        return render_template("auth/local-login.html")

    user = User.query.filter_by(moodle_name=request.args["eleve"]).one_or_none()
    if user is None:
        return render_template("auth/first-login.html", **request.args)
    return render_template("auth/login.html", user=user)


@bp.route('/signin', methods=("POST",))
def signin() -> str|Response:
    if "login" in request.form and "passwd" in request.form:
        user:User|None = User.connect(request.form["login"], request.form["passwd"])
        if user is not None:
            flash("Connecté avec succès")
            return redirect(url_for("hello"))
        else:
            flash("Identifiant ou mot de passe incorrect")
            return redirect(url_for("profile.login"))
    return "NotImplemented"


@bp.route('/signup', methods=("POST",))
def signup() -> str|Response:    
    if User.query.filter(User.role == Role.ADMIN).first() is None:
        user = User(moodle_name=request.form["login"], password=request.form["passwd"], role=Role.ADMIN)  # type: ignore
        db.session.add(user)
        db.session.commit()

        return render_template("auth/first-signup.html")    
        
    return"signup"


@bp.route('/logout')
def logout() -> str|Response:
    User.disconnect()
    return "logout"