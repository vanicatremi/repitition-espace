from datetime import UTC, datetime
from flask import Blueprint, Response, abort, jsonify, render_template, request, session, url_for
from fsrs import FSRS, Rating

from .db import LearningState, db, Card, User

bp = Blueprint("train", __name__, url_prefix="/training")

@bp.route("/")
def train() -> Response|str:
    user:User = User.user_or_401()

    etat:LearningState|None = LearningState.query.filter_by(user=user).filter(LearningState.due < datetime.now(UTC)).order_by(LearningState.due).first()
    if etat is None:
        return Response("Tout fait")
    else:
        return render_template("training/train.html", card=etat.card, url_answer=url_for("train.result", id=etat.id))
    
RATING = {
    "again": Rating.Again,
    "hard": Rating.Hard,
    "good": Rating.Good,
    "easy": Rating.Easy
}

@bp.route("/result/<int:id>", methods=("POST",))
def result(id:int) -> Response:
    user:User = User.user_or_401()

    state:LearningState = db.get_or_404(LearningState, id)
    if state.user_id == user.id:
        abort(403)
    
    repetition = state.to_fsrs()
    repeater = FSRS()
    scheduling_cards = repeater.repeat(repetition, datetime.now(UTC))
    info = scheduling_cards[RATING[request.form["level"]]]
    state.update_from_fsrs(info.card)

    db.session.commit()

    return jsonify({ "status": "OK" })