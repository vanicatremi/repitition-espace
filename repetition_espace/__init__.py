import os

from flask import Flask, flash, redirect, render_template, request, session, url_for
from werkzeug.wrappers import Response

import markdown
from markupsafe import Markup

from .db import db, init_db_command
from . import profile, questions, select, training



def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        SQLALCHEMY_DATABASE_URI=f"sqlite:///{app.instance_path}/project.db"
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # db init, sqlite for now, to be configurable
    db.init_app(app)
    app.cli.add_command(init_db_command)

    # a simple page that says hello
    @app.route('/')
    @app.route('/index.html')
    def hello() -> Response|str:
        if "user" not in session:
            return redirect(url_for("profile.login", **request.args)) # type: ignore

        return render_template("index.html")
    
    @app.errorhandler(401)
    def error_401(error) -> tuple[Response|str, int]:
        flash("vous devez être connecter pour voir cette ressource", "error")
        return render_template("auth/local-login.html"), 401
    
    @app.template_filter('markdown')
    def markdown_filter(s):
        return Markup(markdown.markdown(s, extensions=['codehilite', 'extra']))
    
    app.register_blueprint(questions.bp)
    app.register_blueprint(select.bp)
    app.register_blueprint(training.bp)
    app.register_blueprint(profile.bp)

    return app
