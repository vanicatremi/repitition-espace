from flask import Blueprint, current_app, flash, g, redirect, render_template, request, session, url_for
from itsdangerous import TimestampSigner

from .db import db, Card, User

bp = Blueprint("cards", __name__, url_prefix="/cards")

@bp.route("/")
def index() -> str:
    user:User = User.user_or_401()
    return render_template("cards/index.html", user=user, cards=Card.query.all())


@bp.route("/new", methods=("GET", "POST"))
def new() -> str:
    user:User = User.user_or_401()
    def message_and_display(message, category="error"):
        flash(message, category="error")
        return render_template("cards/creation.html", user=user, card=card)

    if request.method == "POST":
        if "name" not in request.form or "response" not in request.form or "question" not in request.form:
            raise ValueError("Il manque certain des champs du formulaire")

        card = Card(name=request.form["name"], question=request.form["question"], answer=request.form["response"])

        if request.form["name"].strip() == "":
            return message_and_display("La carte n'a pas de nom")
        if request.form["question"].strip() == "":
            return message_and_display("La carte n'a pas de question")
        if request.form["response"].strip() == "":
            return message_and_display("La carte n'a pas de reponse")
            
        req =  Card.query.where(Card.name == request.form["name"])
        if db.session.execute(req).first() is not None:
            return message_and_display("Le nom est déjà utilisé")
    
        db.session.add(card)
        db.session.commit()
        flash(f"carte « {{ card.name }} » ajoutée", category="info")
        return render_template("cards/preview.html", user=user, card=card, create=True)

    return render_template("cards/creation.html", user=user)


@bp.route("/view/<int:id>")
def view(id:int) -> str:
    user:User = User.user_or_401()
    card = db.get_or_404(Card, id)
    return render_template("cards/preview.html", user=user, card=card, create=False)


@bp.route("/edit/<int:id>", methods=("GET", "POST"))
def edit(id:int) -> str:
    user:User = User.user_or_401()
    card = db.get_or_404(Card, id)
    s = TimestampSigner(current_app.config["SECRET_KEY"], salt="cards.edit")
    if request.method == "POST":
        if "name" not in request.form or "response" not in request.form or "question" not in request.form or "sign.id" not in request.form:
            raise ValueError("Il manque certain des champs du formulaire")
        
        if s.validate(request.form["sign.id"], max_age=60 * 60):
            raise ValueError("mauvaise signature pour l'id")
        
        card.name=request.form["name"]
        card.question=request.form["question"]
        card.answer=request.form["response"]

        db.session.commit()
        flash("Carte mise à jour", category="info")

        
    return render_template("cards/edit.html", user=user, card=card, sign_id = s.sign(str(card.id)), create=False)
