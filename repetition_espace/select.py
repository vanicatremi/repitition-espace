from flask import Blueprint, Response, abort, jsonify, render_template, session, url_for
from sqlalchemy import Exists

from .db import LearningState, db, Card, User

bp = Blueprint("select", __name__, url_prefix="/select")


@bp.route("/")
def index() -> str:
    user:User = User.user_or_401()

    exist_user:Exists = User.query.filter(User.moodle_name == user.moodle_name).filter(LearningState.user_id == User.id).exists()

    req = db.select(Card, exist_user).outerjoin(LearningState)

    return render_template("select/index.html", cards=db.session.execute(req).all(), selected_card=user.repetition)


@bp.route("/select/<int:id>/<int:status>")
def select(id:int, status:bool) -> Response:
    user:User = User.user_or_401()

    card:Card = db.get_or_404(Card, id)
    if status:
        db.session.add(LearningState(user=user, card=card))
        db.session.commit()
    else:
        link = LearningState.query.filter_by(user = user).filter_by(card = card).first()
        db.session.delete(link)
        db.session.commit()
        
    url = url_for('select.select', id=id, status=not(status))

    return jsonify({ "id": id, "status": status, "url": url })